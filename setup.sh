#!/bin/bash

function first_steps() {
    ((CN_TIMES = 5))

    sed -i '3 s/#//g' /etc/apk/repositories

    while :
    do
    if apk add nano docker;
    then
        break
    elif [ "$CN_TIMES" -eq 0 ];
    then
        echo "Network error. Aborting."
        exit
    else
        echo "Network error. Restarting."
        ((CN_TIMES = CN_TIMES - 1))
        continue
    fi
    done

    ((CN_TIMES = 5))

    rc-update add docker
    rc-update add containerd

    while :
    do
    if  [ ! -f /var/run/docker.pid ]; then
        rc-service docker start
        ((CN_TIMES = CN_TIMES - 1))
        sleep 1
        continue
    elif [ "$CN_TIMES" -eq 0 ]; then
        echo "Something wrong happened with docker."
        echo "Try rebooting the machine and start this script again."
        echo "$CN_TIMES"
        exit
    else
        break
    fi
    done

    ((CN_TIMES = 5))

    rc-service containerd start
    sleep 3

    while :
    do
    if docker pull eugensystems/wargame3;
    then
        break
    elif [ "$CN_TIMES" -eq 0 ];
    then
        echo "Netwotk error. Aborting."
        exit
    else
        echo "Network error. Restarting."
        ((CN_TIMES = CN_TIMES - 1))
        sleep 2
        continue
    fi
    done
}

# $1 - имя сервера
# $2 - Логин
# $3 - Ключ из почты
function create_settings() {
    if [ -f "$HOME/server/settings/login.ini" ]; then
        rm -f "$HOME/server/settings/login.ini"
    fi

    if [ -f "$HOME/server/settings/variables.ini" ]; then
        rm -f "$HOME/server/settings/variables.ini"
    fi

    if  [ ! -d "$HOME/server/settings" ]; then
        mkdir -p "$HOME/server/settings"
    else
        rm -rf "$HOME/server/settings"
        mkdir -p "$HOME/server/settings"
    fi

    echo "ServerName = $1" >> "$HOME/server/settings/variables.ini"
    echo 'login="'"$2"'"' >> "$HOME/server/settings/login.ini"
    echo 'dedicated_key="'"$3"'"' >> "$HOME/server/settings/login.ini"
}

# $1 - Минимальное значение
# $2 - Максимальное значение
# $3 - Значение пользователя
function getNormalValue() {
    if [ "$3" -lt "$1" ] 2> /dev/null;
    then
        echo "Too small value try again."
        return 1
    elif [ "$3" -gt "$2" ] 2> /dev/null;
    then
        echo "Too great value try again."
        return 1
    elif [ "$3" -ge "$1" ] 2> /dev/null || [ "$3" -le "$2" ] 2> /dev/null;
    then
        return 0
    else
        echo "Unknown value, try again."
        return 1
    fi
}

function enter_values() {
    while :
    do
        read -r -p "Do you want to use default settings? (y/n): " ST1ANS
        if [ "$ST1ANS" == "y" ] || [ "$ST1ANS" == "n" ];
        then
            if [ "$ST1ANS" == "n" ];
            then
                echo "Choose game mode:"
                echo "0 - NATO vs PACT"
                echo "1 - NATO vs NATO"
                echo "2 - PACT vs PACT"
                while :
                do
                    read -r -p "Choose game mode (0 - 2): " GMODE
                    if getNormalValue 0 2 "$GMODE"; then
                        echo "GameType = $GMODE" >> "$HOME/server/settings/variables.ini"
                        break
                    else
                        continue
                    fi
                done

                while :
                do
                    read -r -p "Enter amount of money for game start (1000 - 10000): " SMONEY
                    if getNormalValue 1000 10000 "$SMONEY"; then
                        echo "InitMoney = $SMONEY" >> "$HOME/server/settings/variables.ini"
                        break
                    else
                        continue
                    fi
                done

                echo "Enter game maximum duration in seconds (1200-14400)"
                echo "0 - unlimited"
                while :
                do
                    read -r -p "Enter game maximum duration in seconds (0, 1200-14400): " GDURAT
                    if [ "$GDURAT" -eq 0 ] 2> /dev/null; then
                        echo "TimeLimit = $GDURAT" >> "$HOME/server/settings/variables.ini"
                        break
                    elif [ "$GDURAT" -ne 0 ] 2> /dev/null; then
                        if getNormalValue 1200 14400 "$GDURAT"; then
                            echo "TimeLimit = $GDURAT" >> "$HOME/server/settings/variables.ini"
                            break
                        else
                            continue
                        fi
                    else
                        continue
                    fi
                done

                while :
                do
                    read -r -p "Enter maximum score value to win (1000 - 10000): " GSCORE
                    if getNormalValue 1000 10000 "$GSCORE"; then
                        echo "ScoreLimit = $GSCORE" >> "$HOME/server/settings/variables.ini"
                        break
                    else
                        continue
                    fi
                done

                echo "Choose game mode:"
                echo "1 - Destruction"
                echo "2 - Economy"
                echo "3 - Conquest"
                while :
                do
                    read -r -p "Choose game mode (1 - 3): " GVICCND
                    if getNormalValue 1 3 "$GVICCND"; then
                        if [ "$GVICCND" -eq 1 ]; then
                            echo "VictoryCond = 1" >> "$HOME/server/settings/variables.ini"
                            break
                        elif [ "$GVICCND" -eq 2 ]; then
                            echo "VictoryCond = 3" >> "$HOME/server/settings/variables.ini"
                            break
                        elif [ "$GVICCND" -eq 3 ]; then
                            echo "VictoryCond = 4" >> "$HOME/server/settings/variables.ini"
                            break
                        else
                            continue
                        fi
                    else
                        continue
                    fi
                done

                while :
                    do
                        read -r -p "Enter warmup countdown (10-60): " GWARMUP
                        if getNormalValue 10 60 "$GWARMUP"; then
                            echo "WarmupCountdown = $GWARMUP" >> "$HOME/server/settings/variables.ini"
                            break
                        else
                            continue
                        fi
                    done

                while :
                do
                    read -r -p "Enter deployment phase max duration in seconds (10 - 60): " GDEPLT
                    if getNormalValue 10 60 "$GDEPLT"; then
                        echo "DeploiementTimeMax = $GDEPLT" >> "$HOME/server/settings/variables.ini"
                        break
                    else
                        continue
                    fi
                done

                echo "Choose income rate: "
                echo "0 - None"
                echo "1 - Very Low"
                echo "2 - Low"
                echo "3 - Normal"
                echo "4 - High"
                echo "5 - Very High"
                while :
                do
                    read -r -p "Choose income rate (0 - 5): " GINCOM
                    if getNormalValue 0 5 "$GINCOM"; then
                        echo "IncomeRate = $GINCOM" >> "$HOME/server/settings/variables.ini"
                        break
                    else
                        continue
                    fi
                done

            fi
            break
        else
            echo "Incorrect answer! Try again."
            continue
        fi
    done


}

if [ "$EUID" -ne 0 ];
then
    echo "This script must be started as root"
    exit
fi

while :
do
    read -r -p "Enter your exposed IP address: " IP
    if [[ $IP =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]];
    then
        break
    else
        echo "Wrong format for IP address, try again"
        continue
    fi
done

while :
do
    read -r -p "Enter your exposed port (1025 - 65535): " PORT
    if getNormalValue 1025 65535 "$PORT"; then
        break
    else
        continue
    fi
done

while :
do
    read -r -p "Your login from E-mail: " LOGIN
    if [ -z "$LOGIN" ];
    then
        continue
    else
        break
    fi
done

while :
do
    read -r -p "Your dedicated key from E-mail: " KEY
    if [[ "$KEY" =~ ^[A-Z0-9]{18,30} ]];
    then
        break
    else
        continue
    fi
done

while :
do
    read -r -p "Enter server's name: " SVNAME
    if [ -z "$SVNAME" ];
    then
        continue
    else
        break
    fi
done

first_steps
create_settings "$SVNAME" "$LOGIN" "$KEY"
enter_values

docker run -d --name wg_server \
--net host \
-e EXPOSEDIP="$IP" \
-e EXPOSEDPORT="$PORT" \
-v "$HOME/server/settings":/server/settings \
--restart=unless-stopped \
eugensystems/wargame3 > /dev/null

echo "Now you can checkout your server with commands:"
echo "docker logs wg_server | less - checks the server logs"
echo "docker stop wg_server - stops the server"
echo "docker restart wg_server - restarting the server"
echo "nano ~/server/settings/variables.ini - change the server settings"
