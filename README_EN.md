# Contents

1. [Description](#1-description)
2. [Preface](#2-preface)
3. [Getting Started](#3-getting-started)
4. [Preparing](#4-preparing)
5. [Start-Up](#5-start-up)
6. [Initial Setup](#6-initial-setup)
7. [After startup](#7-after-startup)

# 1. Description

The script is designed to install and initial installation of a dedicated server for the game Wargame: Red Dragon.

# 2. Preface

Before throwing me overboard for being too anal about entering commands please note that this description is primarily aimed at people who have never in their life touched a UNIX-like system and do not even know the Windows console. This tutorial is intended to be a supplement to [this Steam tutorial](https://steamcommunity.com/sharedfiles/filedetails/?id=2859885324) and is a quicker method for those who don't understand what they need to do.
<br>

# 3. Getting Started

The script is supposed to run in a virtual machine with the Alpine Linux operating system, the Bash shell and the Git version control system installed. If you don't have Bash and/or Git installed on your system, do it with the command:

> `apk add bash git`

# 4. Preparing

The first thing to do is to clone this repository. This is done with a command:

> `git clone https://codeberg.org/buster_daemon/wargame_server.git`

After that, a folder will be created with the name of the repository (*wargame_server*), go to it:

> `cd wargame_server`

In this folder there are several files, but among them only `setup.sh` is needed. To start it must be given the rights to run, this is done by the command:

> `chmod +x ./setup.sh`

# 5. Start-Up

To start it, you have to type:

> ./setup.sh

You will first be asked to enter your public IP address:

> Enter your exposed IP address:

To find it out, you have to go to [2ip](https://2ip.ru) or [whatismyipaddress](https://whatismyipaddress.com) and then enter the value of *Your IP address* or *IPv4* depending on which site you visited.

Next you will be asked to enter the number of the open port you are going to run the server on:

> Enter your exposed port (1025 - 65535):

The value can be any number from 1025 to 65535. For more information on opening and forwarding ports you can find in the above mentioned Steam manual, in your router's manual and/or by googling on the internet.

As for the login from email question, enter your login that you received in your email when you contacted Eugen Systems:

> Your login from E-mail:

After your login, you must enter the key that you also received by e-mail from Eugen Systems:

> Your dedicated key from E-mail:

You must now enter the name of the server. This can be absolutely anything you want as long as it doesn't break the user agreement ~~(*is there even one?*)~~:

> Enter server's name:

Then the Docker container management system, the Nano text editor and the game server image from the developers will be installed automatically. Docker will automatically start each time you turn on the virtual machine. The Nano editor will allow you to change the server configuration after startup.

After the auto-installation you will be offered the choice to use the default settings from the developers (`y`) or to enter your own (`n`). In the first case, the server starts immediately with the default settings. In the second case you will be asked a few questions about the settings:

> Do you want to use default settings? (y/n):

# 6. Initial Setup

The first question is the choice of confrontation mode, of which there are only three:

1. NATO vs. PACT
2. NATO vs. NATO
3. PACT vs. PACT

Three values from 0 to 2 are accepted (*counting from zero*).

> Choose game mode: <br>
> 0 - NATO vs PACT <br>
> 1 - NANO vs NATO <br>
> 2 - PACT vs PACT <br>
> Choose game mode (0 - 2):

The second question is the number of points given to players at the start of the game. Values from 1000 to 10000 are accepted:

> Enter amount of money for game start (1000 - 10000):

Next you'll be asked how long the game should last. Several values are accepted:

1. 0 - The game lasts indefinitely
2. Range from 1200 to 14400 (*value is given in seconds*)

To convert to minutes, divide the desired value by 60:

> Enter game maximum duration in seconds (0, 1200-14400):

After that you must enter the number of points needed to win. A value in the range from 1000 to 10000 is accepted:

> Enter maximum score value to win (1000 - 10000):

Now you will be asked what game mode should work. There are three values in total:

1. Destruction
2. Economy
3. Conquest

> Choose game mode: <br>
> 1 - Destruction <br>
> 2 - Economy <br>
> 3 - Conquest <br>
> Choose game mode (1 - 3):

Then you must choose the time (in seconds) before the start of the game, when there are enough players in the lobby. A value from 10 to 60 is accepted:

> Enter warmup countdown (10-60):

The next item is the time (in seconds) of the preparatory phase during the game, when users deploy their units at the start. A value between 10 and 60 is accepted:

> Enter deployment phase max duration in seconds (10 - 60):

Next, you need to choose the rate of accumulation of points, which has six levels:

1. No income
2. Very Low
3. Low
4. Normal
5. High
6. Very High

Values from 0 to 5 are accepted:

> Chose income rate: <br>
> 0 - None <br>
> 1 - Very Low <br>
> 2 - Low <br>
> 3 - Normal <br>
> 4 - High <br>
> 5 - Very High <br>
> Choose income rate (0 - 5):

And this ends the configuration. The server starts in the background with all the specified parameters.

# 7. After startup

You can check the status of the server with the command:

> `docker logs wg_server | less`

Since the logs are not overwritten, in order to go to the end of the logs you have to click on *End*, at the beginning on *Home*, up and down with the corresponding arrows.

You can also check the status of the server with the command:

> `docker ps`

If you get a line that mentions a container called `wg_server` then everything is fine.

To change the settings manually, you need to open the file in which the server parameters are stored:

> `nano $HOME/server/settings/variables.ini`

All allowable parameters and values are mentioned in the same Steam manual and in the official documentation. <br>
After making the changes, the server must be restarted with the command

> `docker restart wg_server`
